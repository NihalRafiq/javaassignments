package Assignments;

import java.util.Arrays;
import java.util.Scanner;

public class MaxMin{
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
    
        System.out.println("Enter the numbers");
        int arr[]=new int[5];
        int sum=0;

        for(int i=0;i<5;i++)
        {
            arr[i] = sc.nextInt();
            sum+=arr[i];           
        }
        
        Arrays.sort(arr);
        int min=arr[4];
        int max=arr[0];
        int minSum = sum-min;
        int maxSum = sum-max;
        System.out.println("Minimum value :" + minSum);
        System.out.println("Maximum value :" + maxSum);
    }
   }

