package Jardemo;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Jardemo.EmployeeDetails.EmpDetails;
// writing json
public class ObjtoJson {

public static void main(String[] args) {

//Gson gson= new Gson();
Gson gson= new GsonBuilder().setPrettyPrinting().create();
EmployeeDetails ed = createmyObject();
System.out.println(ed); // java object printed
String j = gson.toJson(ed); // to convert java object to JSON
System.out.println(j);


try(FileWriter writer = new FileWriter("C:\\Users\\241317\\Java-workspace\\java1.json")){
gson.toJson(ed, writer);
//System.out.println("ok its done go and check your myfile.json");
}
catch(IOException e) {
e.printStackTrace();
}
}
private static EmployeeDetails createmyObject() {
EmployeeDetails ed= new EmployeeDetails();
ed.setName("Michael clarke");
ed.setAge(55);
ed.setCity("Melbourne");
return ed;
}

}